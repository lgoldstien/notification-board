var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser')
var _ = require('underscore');
var fs = require('fs');
var save = require('save');
var saveJson = require('save-json');
var generate = require('project-name-generator');

var screenStore = save('Screen', { engine: saveJson(__dirname + '/data/screens.json') });
var monitorStore = save('Screen', { engine: saveJson(__dirname + '/data/monitors.json') });

var app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());

var notificationHTML = "";
var websiteURI = "";
var watermark = "";

require.extensions['.html'] = function (module, filename) {
    module.exports = fs.readFileSync(filename, 'utf8');
};

var templates = {
    layout: require('./templates/layout.html'),
    admin: require('./templates/admin.html'),
    admin_screen: require('./templates/admin_screen.html'),
    index: require('./templates/index.html'),
    screen_name: require('./templates/screen_name.html')
};

var _default_view_variables = {
    screens: [],
    monitors: [],
    message: null
};

var renderTemplate = function (template, variables) {
    variables = _.extend(_default_view_variables, variables);
    var internal = _.template(templates[template])(variables);
    return _.template(templates.layout)(_.extend({ internal_content: internal }, variables));
};

app.get('/', function (req, res) {
    var name = null;
    if (!_.isUndefined(req.cookies.screen_name) && req.cookies.screen_name != "undefined") {
        name = req.cookies.screen_name;
    } else {
        name = generate().dashed;
    }
    monitorStore.findOne({ name: name }, function (err, monitor) {
        var mon = { name: name, last_seen: Date.now() };
        if (! _.isUndefined(monitor)) {
            mon.id = monitor.id;
        }
        monitorStore.createOrUpdate(mon, function(err, monitor) {
            if (monitor.screen) {
                res.redirect(monitor.screen);
            } else {
                res.cookie('screen_name', name);
                res.send(renderTemplate('screen_name', {
                    name: name
                }));
            }
        });
    });
});

app.get('/admin', function (req, res) {
    screenStore.find({}, function (err, screens) {
        monitorStore.find({}, function (err, monitors) {
            res.send(renderTemplate('admin', {
                screens: screens,
                monitors: monitors
            }));
        });
    });
});

app.put('/admin', function (req, res) {
    monitorStore.findOne({ name: req.body.monitor }, function (err, monitor) {
        monitor = _.extend(monitor, {
            screen: req.body.screen
        });
        monitorStore.createOrUpdate(monitor, function(err, monitor) {
            res.send("done");
        });
    });
});

app.post('/admin', function (req, res) {
    var screen = req.body.screen;
    screenStore.find({ name: screen }, function (err, screen_data) {
        if (! screen_data) {
            screenStore.create({ name: screen }, function (err, screen) {
                screenStore.find({}, function (err, screens) {
                    monitorStore.find({}, function (err, monitors) {
                        res.send(renderTemplate('admin', {
                            screens: screens,
                            monitors: monitors,
                            message: null
                        }));
                    });
                });
            });
        } else {
            screenStore.find({}, function (err, screens) {
                monitorStore.find({}, function (err, monitors) {
                    res.send(renderTemplate('admin', {
                        screens: screens,
                        monitors: monitors,
                        message: {
                            text: "Screen already exists with that name",
                            style: "danger"
                        }
                    }));
                });
            });
        }
    });
});

app.get('/admin/:screen', function (req, res) {
    screenStore.findOne({ name: req.params.screen }, function (err, screen) {
        if (!screen) {
            res.redirect('/admin');
        } else {
            res.send(renderTemplate('admin_screen', {
                website: screen.website || "",
                html: screen.html || "",
                watermark: screen.watermark || ""
            }));
        }
    });
});

app.post('/admin/:screen', function (req, res) {
    var screen_name = req.params.screen;
    var html = req.body.html;
    var website = req.body.website;
    var watermark = req.body.watermark;

    screenStore.findOne({ name: screen_name }, function (err, screen) {
        var screen_data = { name: screen_name, last_seen: Date.now(), html: html, website: website, watermark: watermark };
        if (! _.isUndefined(screen)) {
            screen_data.id = screen.id;
        }
        screenStore.createOrUpdate(screen_data, function(err, screen) {
            res.send(renderTemplate('admin_screen', { website: screen.website, html: screen.html, watermark: watermark }));
        });
    });
});


app.get('/:screen', function (req, res) {
    var screen_name = req.params.screen;
    var monitor_name = null;
    if (!_.isUndefined(req.cookies.screen_name) && req.cookies.screen_name != "undefined") {
        monitor_name = req.cookies.screen_name;
    } else {
        res.redirect('/');
    }

    monitorStore.findOne({ name: monitor_name }, function (err, monitor) {
        if (monitor.screen != screen_name) {
            res.redirect('/');
        } else {
            screenStore.findOne({ name: req.params.screen }, function (err, screen) {
                if (!screen) {
                    res.redirect('/');
                } else {
                    res.send(renderTemplate('index', {
                        website: screen.website || "",
                        html: screen.html || "",
                        watermark: screen.watermark || ""
                    }));
                }
            });
        }
    });


});

app.listen(3000);
