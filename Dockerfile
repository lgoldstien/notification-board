FROM node:5.1

ENV APP_PORT=3000

COPY package.json /src/package.json
RUN cd /src; npm install

COPY . /src

EXPOSE ${APP_PORT}
CMD ["node", "/src/index.js"]

